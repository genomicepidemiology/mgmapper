# Databases to be use with the MGmapper programs.
# Column 1: Full path to database - exclude suffix names to a database
# Column 2: Name of database to be used in output - can be any name you want. Names in column 2 must be unique.
# Column 3- optional description of the database e.g. date of creation, number of fasta entries, size  and other remarks
# The 3 columns are white space separated.
# The first database phiX174 must be present in the first line - it will not show up when running MGmapper_PE.pl -h or MGmapper_SE.pl -h
# The phiX174 database will be used as first step to clean reads for potential positive control reads
#
/home/databases/metagenomics/db/phiX174/phiX174	phiX174	
/home/databases/metagenomics/db/bacteria/bacteria	Bacteria
/home/databases/metagenomics/db/archaea/archaea	  Archaea
/home/databases/metagenomics/db/MetaHitAssembly/MetaHitAssembly	MetaHitAssembly
/home/databases/metagenomics/db/HumanMicrobiome/HumanMicrobiome	HumanMicrobiome
/home/databases/metagenomics/db/bacteria_draft/bacteria_draft	Bacteria_draft
/home/databases/metagenomics/db/ResFinder/ResFinder		ResFinder
/home/databases/metagenomics/db/Human/Human			Human
/home/databases/metagenomics/db/virus/virus			Virus
/home/databases/metagenomics/db/fungi/fungi			Fungi
/home/databases/metagenomics/db/protozoa/protozoa		Protozoa
/home/databases/metagenomics/db/plasmid/plasmid			Plasmid
/home/databases/metagenomics/db/Virulence/Virulence		Virulence
/home/databases/metagenomics/db/plant/plant			Plant
/home/databases/metagenomics/db/vertebrate_mammals/vertebrate_mammals	Vertebrates_mammals
/home/databases/metagenomics/db/vertebrate_other/vertebrate_other	Vertebrates_other
/home/databases/metagenomics/db/invertebrate/invertebrate		Invertebrates
/home/databases/metagenomics/db/GreenGenes/GreenGenes			GreenGenes
/home/databases/metagenomics/db/Silva/Silva				Silva
/home/databases/metagenomics/db/nt/nt					nt
/home/databases/metagenomics/db/MegaVirales/MegaVirales			MegaVirales
/home/databases/metagenomics/db/IGC_20160927/IGC			IGC
#
# Databases are made by running these 3 programs
# fastauniq.pl -i databaseName.fsa -v -l fastauniq.databaseName.log -o databaseName
# bwa index databaseName
# samtools faidx databaseName
